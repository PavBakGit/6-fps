using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/HeardSoundDecision")]
public class HeardSoundDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return HeardSound(controller);
    }

    /// <summary>
    /// Verifica si hay sonidos en la lista de sonidos
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool HeardSound(StateController controller)
    {
        return (controller.hearedSounds.Count > 0);
    }
}
