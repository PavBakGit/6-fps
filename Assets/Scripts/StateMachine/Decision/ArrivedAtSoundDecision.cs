using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/ArrivedAtSound")]
public class ArrivedAtSoundDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return Arrived(controller);
    }

    /// <summary>
    /// Devuelve si ha alcanzado el destino
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool Arrived(StateController controller)
    {
        return (controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance);
    }
}
