using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "AISystem/Decisions/Cover")]
public class CoverDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return CheckLifeToCover(controller);
    }

    /// <summary>
    /// Verifica si la vida es lo suficiente baja como para iniciar la b�squeda de cobertura
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool CheckLifeToCover(StateController controller)
    {

        return (controller.enemyHealth.currentLife <= controller.enemyStats.lifeCoverThreshold);
    }
}
