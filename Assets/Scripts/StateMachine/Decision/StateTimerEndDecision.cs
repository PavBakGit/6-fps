using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/StateTimerEndDecision")]
public class StateTimerEndDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return CheckTimer(controller);
    }

    /// <summary>
    /// Verifica si ha terminado la cuenta atras
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool CheckTimer(StateController controller)
    {
        return (controller.stateTimer <= 0);
    }
}
