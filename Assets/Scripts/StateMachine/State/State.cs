using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/State")]
public class State : ScriptableObject
{
    // Listado de acciones que realizará el estado
    public StateAction[] actions;
    // Listado de transiciones a evaluar
    public Transition[] transitions;

    // Texto de estado para debug
    public string stateText;
    // Texto de estado para debug
    public Color stateColor = Color.green;
    // Distancia de parada del estado
    public float stoppingDistance = 2f;

    /// <summary>
    /// Actualiza el estado
    /// </summary>
    /// <param name="controller"></param>
    public void UpdateState(StateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    /// <summary>
    /// Realizamos todas las acciones del estado
    /// </summary>
    /// <param name="controller"></param>
    public void DoActions(StateController controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(controller);
        }
    }

    /// <summary>
    /// Evalua las decisiones de las transiciones
    /// </summary>
    /// <param name="controller"></param>
    private void CheckTransitions(StateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            // Almacenamos en una variable el resultado de la decisión
            bool decisionSucceded = transitions[i].decision.Decide(controller);

            if(decisionSucceded)
            {
                // Si el resultado es positivo, cambiamos al estado configurado para el resultado true
                controller.TransitionToState(transitions[i].trueState);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }

            // Si el valor ha cambiado, significa que se ha producido una transición, por lo que cortaremos otras posibles transiciones que pueden romper la lógica configurada
            if(controller.currentState != this)
            {
                break;
            }
        }
    }
}
