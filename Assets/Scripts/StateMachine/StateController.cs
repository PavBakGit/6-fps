using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class StateController : WeaponShooter
{
    public bool debugMode = false;
    // Estado actual de la m�quina de estados
    public State currentState;

    // Referencia al navmeshagent
    [HideInInspector] public NavMeshAgent navMeshAgent;
    // Listado de waypoints disponibles para la patrulla
    public List<Transform> wayPointList;
    // Siguiente waypoint a desplazarse
    public int nextWaypoint;

    // Para desactivar la IA en caso de ser necesario
    public bool aiActive = true;

    // Estad�sticas del enemigo
    public EnemyStats enemyStats;
    // Posici�n y rientaci�n del ojo
    public Transform eye;
    // Transform del objetivo
    public Transform target;
    // �ltima posici�n en la que fue avistado el objetivo
    public Vector3 lastSpottedPosition;

    public float stateTimer;
    public bool timerCounting;

    // Referencia al text3D para el estado de debug
    public TextMesh stateText;
    // Referencia al componente que gestiona la vida del enemigo
    public EnemyHealth enemyHealth;

    // Listado de sonidos escuchados
    public List<Vector3> hearedSounds;
    // Posici�n del sonido a verificar actual
    public Vector3 currentSoundPosition;


    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        // Si no est� activa la IA salimos del m�todo sin hacer nada
        if (!aiActive) return;
        // Realizamos el update del state indicando que seremos nosotros el controller a utilizar
        currentState.UpdateState(this);

        
    }

    private void OnDrawGizmos()
    {
        if (!debugMode) return;
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(lastSpottedPosition, 0.5f);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(currentSoundPosition, 0.5f);
    }

    /// <summary>
    /// Cambia la m�quina de estados al estado indicado
    /// </summary>
    /// <param name="nextState"></param>
    public void TransitionToState(State nextState)
    {
        // Si el pr�ximo estado es distinto al actual
        if(nextState != currentState)
        {
            // Hacemos efectivo el cambio de estado
            currentState = nextState;
            // Reseteamos el contador de temporizador
            stateTimer = 0f;
            timerCounting = false;
            // Recuperamos el texto a mostrar para indicador el estado
            stateText.text = currentState.stateText;
            // Recuperamos el color a mostrar para el indicador de estado
            stateText.color = currentState.stateColor;
            // Ajusto el stopping distance para el estado
            navMeshAgent.stoppingDistance = nextState.stoppingDistance;
            //
            enemyHealth.healingTimer = Time.time + enemyStats.healingTick;
        }
    }

    /// <summary>
    /// Agrega una posici�n a la lista de sonidos escuchados
    /// </summary>
    /// <param name="soundPosition"></param>
    public void HearSound(Vector3 soundPosition)
    {
        // Si el sonido recibido se encuentra dentro del rango de audici�n
        if(Vector3.Distance(soundPosition, transform.position) <= enemyStats.hearRange)
        {
            // Lo agregamos a la lista
            hearedSounds.Add(soundPosition);
        }
    }
}
