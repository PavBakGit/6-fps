using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/Patrol")]
public class PatrolAction : StateAction
{
    // Para activar la patrulla aleatoria entre los puntos configurados
    public bool randomPatrol = false;
    public override void Act(StateController controller)
    {
        Patrol(controller);
    }

    /// <summary>
    /// Realiza las acciones de cambio de waypoints y gesti�n del navmesh para la patrulla
    /// </summary>
    /// <param name="controller"></param>
    private void Patrol(StateController controller)
    {
        // Recuperamos el siguiente waypoint y lo establecemos como destino
        controller.navMeshAgent.destination = controller.wayPointList[controller.nextWaypoint].position;
        // Iniciamos movimiento hacia el destino
        controller.navMeshAgent.isStopped = false;
        // Cambiamos a velocidad de patrulla
        controller.navMeshAgent.speed = controller.enemyStats.patrolSpeed;

        // Si la distancia al objetivo es ingerior al stopping distance, considetamos haber llegado al waypoint
        if(controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance && !controller.navMeshAgent.pathPending)
        {
            if(randomPatrol)
            {
                // Elegimos un waypoint aleatorio de la lista
                controller.nextWaypoint = Random.Range(0, controller.wayPointList.Count);
            }
            else
            {
                controller.nextWaypoint = (controller.nextWaypoint + 1) % controller.wayPointList.Count;
            }
        }
    }
}
