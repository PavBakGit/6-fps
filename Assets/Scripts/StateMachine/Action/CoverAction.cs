using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/Cover")]
public class CoverAction : StateAction
{
    // Para localizar puntos de cobertura cercanos
    public LayerMask coverLayer;
    // Para evaluar si hay oclusi�n de cobertura
    public LayerMask occlusionLayer;

    public override void Act(StateController controller)
    {
        Cover(controller);
    }

    /// <summary>
    /// Busca y se desplaza a la cobertura
    /// </summary>
    /// <param name="controller"></param>
    private void Cover(StateController controller)
    {
        controller.enemyHealth.newDamageOrigin = false;
        // Buscamos puntos de cobertura en torno a la IA
        Collider[] coverPoints = Physics.OverlapSphere(controller.transform.position, controller.enemyStats.coverRange, coverLayer);

        // inicializamos en -1, por si no localizamos ninguno
        float shortestDistance = -1f;
        float tempDistance;
        // Para almacenar la cobertura seleccionada
        Collider selectedCover = new Collider();

        // Recorremos todos los puntos de cobertura disponibles
        foreach (Collider cover in coverPoints)
        {
            // Si se produce un impacto entre el punto de cobertura y el agresor significa que la cobertura es v�lida para esconderse
            if(Physics.Linecast(cover.transform.position, controller.enemyHealth.takingDamageFrom.position, occlusionLayer))
            {
                // Determina la distancia entre la IA y el punto de cobertura
                tempDistance = Vector3.Distance(controller.transform.position, cover.transform.position);

                // Si la distanciaa obtenida es menor que la menor distancia actual
                // o si la distancia m�nima es negativa (indicando que no habia cobertura previa)
                if(tempDistance < shortestDistance || shortestDistance < 0)
                {
                    // Fijamos esta cobertura, como la mejor opci�n actual
                    shortestDistance = tempDistance;
                    selectedCover = cover;
                }
            }
        }
        // Si al finalizar el bucle, la menor distancia es mayor que 0
        // significar� que hemos localizado una cobertura
        if(shortestDistance > 0)
        {
            // Fijamos el destino de desplazamiento de la IA al punto de cobertura
            controller.navMeshAgent.SetDestination(selectedCover.transform.position);
            // Iniciamoss el movimiento hacia el destino
            controller.navMeshAgent.isStopped = false;
        }
    }
}
