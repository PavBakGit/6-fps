using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateSound
{
    /// <summary>
    /// Simula un sonido con la posici�n y rango indicados, informando a la IA de que se encuentre dentro de dicho rango
    /// </summary>
    /// <param name="position"></param>
    /// <param name="range"></param>
    public static void Generate(Vector3 position, float range)
    {
        Collider[] cols = Physics.OverlapSphere(position, range);

        foreach (Collider col in cols)
        {
            StateController controller = col.GetComponent<StateController>();
            if(controller != null)
            {
                controller.HearSound(position);
            }
        }
    }
}
