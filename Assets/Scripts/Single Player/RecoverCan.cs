using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecoverCan : MonoBehaviour
{
    #region VARIABLES
    public float heightToRecover = -10f;
    private Vector3 startPosition;
    private Quaternion startRotation;
    private Rigidbody rigidBody;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y < heightToRecover)
        {
            rigidBody.velocity = Vector3.zero;
            rigidBody.angularVelocity = Vector3.zero;
            transform.position = startPosition;
            transform.rotation = startRotation;
        }
    }
}
