using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorLock : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Bloquea el cursor para que no pueda salir de la ventana
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Cancel"))
        {
            // Si el cursor est� bloqueado
            if(Cursor.lockState == CursorLockMode.Locked)
            {
                // Liberamos el cursor
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                // Si no est� bloqueado, lo bloqueamos
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
        #if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        #endif
    }


}
