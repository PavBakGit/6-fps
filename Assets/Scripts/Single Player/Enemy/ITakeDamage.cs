using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITakeDamage
{
    /// <summary>
    /// Aplica el da�o recibido, informando del origen del da�o
    /// </summary>
    /// <param name="damage"></param>
    /// <param name="origin"></param>
    /// <returns></returns>
    bool TakeDamage(int damage, Transform origin, Vector3 hitPoint);
}
