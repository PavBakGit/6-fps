using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FPSController : WeaponShooter
{
    #region VARIABLES
    [Header("Movement")]
    // Velocidad de desplazamiento
    public float speed = 2f;
    public float runSpeed = 2.5f;
    // Referencia al componente character controller
    private CharacterController player;
    // Variable temportal donde almacenaremos el valor de los ejes de control
    private float moveHorizontal;
    private float moveVertical;
    // Variables temporales para almacenar el valor de la rotaci�n
    private float rotationHorizontal;
    private float rotationVertical = 0f;
    // Vector 3 que indicar� la direcci�n de desplazamiento
    private Vector3 movement = new Vector3();
    
    [Header("Camera")]
    // Transform de cabeza
    public Transform head;
    // Booleana para indicar si el control de rotaci�n vertical estar� o no invertido
    public bool invertView = false;
    // Velocidad de rotaci�n
    public float sensitivity = 2f;
    
    [Header("Jump")]
    // Fuerza con la que se realizar� el salto
    public float jumpForce = 5f;
    // Variable para controlar la velocidad vertical
    private float verticalV;

    [Header("Crouch and run")]
    // Para indicar si el jugador se ecnuentra agachado
    bool isCrouched = false;
    bool isRunning = false;
    // Altura normal
    public float regularHeight = 2f;
    // Altura agachado
    public float crouchedHeight = 1.6f;
    // Para comprobar si se ecncontraba tocando el suelo en el ciclo anterior
    private bool previouslyGrounded = true;

    [Header("Sound")]
    // Distancia que deber� recorrer el player para que suene una pisada
    public float stepDistance = 1f;
    // Clip de sonido de la pisada
    public AudioClip stepSound;
    // Contador de distancia recorrida tras el �ltimo paso
    private float stepDistanceCounter = 0f;
    // Referencia al audiosource
    private AudioSource audioSource;
    // Sonido para salto
    public AudioClip jumpSound;
    // Sonido de aterrizaje
    public AudioClip landingSound;
    // Distancia m�nima de ca�da, para reproducir el sonido de aterrizaje
    public float minVerticalDistanceSound = 1f;
    // Contador interno de distancia vertical recorrida en ca�da
    private float verticalDistanceCounter = 0f;

    // Rango de distancia al que llegara el sonido generado por nuestros pasos
    public float loudSoundRange = 7f;
    // 
    public float quietSoundRange = 2f;

    [Header("Weapon")]
    // Referencia al punto de mira
    public GameObject cross;
    // Modificador para disparo autom�tico
    public bool fullAuto = false;
    
    // Campos de texto para mostrado de munici�n en el HUD
    public Text currentAmmoText;
    public Text maxAmmoText; 
    

    [Header("Physics")]
    // Fuerza al empujar
    public float pushPower = 1f;



    // Referencia al animator
    public Animator animator;

    private WeaponShoot weapon;
    #endregion
    #region EVENTS
    void Start()
    {
        player = GetComponent<CharacterController>();
        audioSource = GetComponent<AudioSource>();
        weapon = GetComponentInChildren<WeaponShoot>();
    }

    void Update()
    {
        // Situamos el Applygravity() primero para asegurarnos que disponemos de isgrounded para el resto del ciclo
        Applygravity();
        Controls();
        Rotation();
        Movement();
        AmmoUpdate();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        // intentamos recuperar un rigidbody del objeto colisionado
        Rigidbody body = hit.collider.attachedRigidbody;

        // Si no hay rigidbody o el objeto es kinem�tico, no hacemos nada
        if(body == null || body.isKinematic)
        {
            return;
        }

        // Elimino el componente vertical del desplazamiento del character controller
        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0f, hit.moveDirection.z);

        // Aplicamos una fuerza al rigidbody conlisionado, con la direcci�n de desplazamiento del character controller multiplicada por la fuerza configurada
        body.AddForce(pushDir * pushPower * Time.deltaTime);
    }
    #endregion
    #region METHODS
    /// <summary>
    /// Identificamos los inputs utilizados
    /// </summary>
    private void Controls()
    {
        moveHorizontal = Input.GetAxisRaw("Horizontal");
        moveVertical = Input.GetAxisRaw("Vertical");
        // Recuperamos el desplazamiento del rat�n para realizar la rotaci�n
        rotationHorizontal = Input.GetAxis("Mouse X") * sensitivity;
        rotationVertical += Input.GetAxis("Mouse Y") * sensitivity * (invertView?1:-1);
        rotationVertical = Mathf.Clamp(rotationVertical, -90f, 90f);
        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }
        if (Input.GetButtonDown("Crouch") && !isRunning)
        {
            AltCrouch();
        }
        if (Input.GetButtonDown("Run") && player.isGrounded)
        {
            Run();
        }
        if (Input.GetButtonUp("Run") && isRunning)
        {
            Run();
        }

        if(!fullAuto && Input.GetButtonDown("Fire1"))
        {
            OnShoot?.Invoke(head.transform);
        }

        if(fullAuto && Input.GetButton("Fire1"))
        {
            OnShoot?.Invoke(head.transform); 
        }

        if(Input.GetButtonDown("Fire2"))
        {
            cross.SetActive(false);
            OnAim?.Invoke(true);
        }

        if(Input.GetButtonUp("Fire2"))
        {
            cross.SetActive(true);
            OnAim?.Invoke(false);
        }

        if (Input.GetButtonDown("Reload"))
        {
            OnReload?.Invoke();
        }
    }

    /// <summary>
    /// Aplica el movimiento en base a los inputs
    /// </summary>
    private void Movement()
    {
        // Asignamos el vector de movimiento recuperado de los input
        movement.Set(moveHorizontal, 0, moveVertical);
        // Normalizamos el vector, para evitar que las diagonales sean mayr que 1.
        movement = movement.normalized;
        // Orientamos el vector de movimiento alin�andolo con player
        movement = transform.rotation * movement;

        // Le indicamos al character controller, que realizamos un desplazamiento, aportando el vector de direcci�n y la magnitud en base a la velocidad
        player.Move(movement * speed * Time.deltaTime);

        stepDistanceCounter += movement.magnitude * speed * Time.deltaTime;
        
        // Si la distancia recorrida es igual o superior a la distancia de una zancada
        if((stepDistanceCounter >= stepDistance) && previouslyGrounded)
        {
            // Reseteamos contador
            stepDistanceCounter = 0f;
            // Hacemos pitch aleatorio al reproducir el sonido
            audioSource.pitch = UnityEngine.Random.Range(0.9f, 1f);
            // Reproducimos el sonido de la pisada
            audioSource.PlayOneShot(stepSound);
            // Generamos un sonido audible por la IA con in rango dependiente de si estamos o no corriendo
            GenerateSound.Generate(transform.position, isRunning ? loudSoundRange : quietSoundRange);
            
        }
    }

    /// <summary>
    /// Rotaci�n vertical y horizontal
    /// </summary>
    private void Rotation()
    {
        // Rotaci�n horizontal todo el cuerpo
        transform.Rotate(0, rotationHorizontal, 0);

        // Rotamos verticalmente la cabeza del jugador
        head.localEulerAngles = new Vector3(rotationVertical, 0, 0);
    }

    /// <summary>
    /// Aplica la gravedad simulada
    /// </summary>
    private void Applygravity()
    {
        if(!previouslyGrounded)
        {
            if(verticalV < 0)
            {
                verticalDistanceCounter += Mathf.Abs(verticalV) * Time.deltaTime;
            }
        }
        else
        {
            // Si en el ciclo anterior estaba grounded, reseteo el contador
            verticalDistanceCounter = 0;
        }
        // Aplicamos la velocidad vertical
        player.Move(Vector3.up * verticalV * Time.deltaTime);

        if(player.isGrounded)
        {
            verticalV = Physics.gravity.y;
            // Si en el momento de tocar el suelo, se ha recorrido una distancia de ca�da suficiente reproducimos el sonido de aterrizaje
            if(verticalDistanceCounter >= minVerticalDistanceSound)
            {
                audioSource.PlayOneShot(landingSound);
            }
        }
        else
        {
            verticalV += Physics.gravity.y * Time.deltaTime;
            verticalV = Mathf.Clamp(verticalV, -50f, 50f);
        }

        previouslyGrounded = player.isGrounded;
    }
    /// <summary>
    /// Realiza un salto dambiando la velocidad vertical
    /// </summary>
    private void Jump()
    {
        // Si esta tocando el suelo
        if(player.isGrounded)
        {
            // Cambio su velocidad vertical para que se asigne la del salto
            verticalV = jumpForce;
            // Cuando saltamos reseteamos el contador de distancia recorrida
            stepDistanceCounter = 0f;
            audioSource.PlayOneShot(jumpSound);
        }
    }

    /// <summary>
    /// Alternador de estado agachado
    /// </summary>
    private void AltCrouch()
    {
        // Negamos el valor actual de la booleana
        isCrouched = !isCrouched;
        player.height = (isCrouched ? crouchedHeight : regularHeight);
        animator.SetBool("Crouched", isCrouched);
    }
    private void Run()
    {
        if (isCrouched)
        {
            AltCrouch();
        }
        isRunning = !isRunning;
        speed = (isRunning ? speed * runSpeed : speed / runSpeed);
    }

    /// <summary>
    /// Actualiza el HUD de munici�n del jugador
    /// </summary>
    public void AmmoUpdate()
    {
        currentAmmoText.text = weapon.magazine.ToString();
        maxAmmoText.text = weapon.magazineSize.ToString();
    }
    #endregion
}
